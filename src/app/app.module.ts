import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');


import {NgModule, ErrorHandler} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { StationService } from './services/station-service/station.service';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import { StationMapComponent } from './station-map/station-map.component';
import { ListStationComponent } from './list/list-station/list-station.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { DetailStationComponent } from './detail/detail-station/detail-station.component';
import { ListSensorComponent } from './list/list-sensor/list-sensor.component';
import { SensorService} from './services/sensor-service/sensor.service';
import { DetailSensorComponent } from './detail/detail-sensor/detail-sensor.component';
import { AqiComponent } from './aqi/aqi.component';
import { AqiGraphComponent } from './graph/aqi-graph/aqi-graph.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { EmailService } from './services/email-service/email.service';
import { EqualValidator } from './validator/equal-validator.directive';
import { RegisterService } from './services/register-service/register.service';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { StationInfoComponent } from './station-info/station-info.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AuthService } from './services/auth-service/auth.service';
import { LoginService } from './services/login-service/login.service';
import { EmailSubscriptionComponent } from './email/email-subscription/email-subscription.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import {UserService} from './services/user-service/user.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AuthHttpInterceptor } from './auth-http-interceptor';
import { RequestInterceptor } from './services/error-service/http_interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import {CommentService} from './services/comment-service/comment.service';
import {CommentComponent} from './comment/comment.component';
import { GlobalErrorHandlerService } from './services/error-service/global-error-handler.service';
import { WINDOW_PROVIDERS } from './services/window-service/window.service';
import { ErrorLogComponent } from './error-log/error-log.component';
import { ErrorLoggingService } from './services/error-service/error-logging.service';
import { ListAlertComponent } from './list-alert/list-alert.component';
import { AlertService} from './services/alert-service/alert.service';

@NgModule({
  declarations: [
    AppComponent,
    StationMapComponent,
    ListStationComponent,
    DetailStationComponent,
    ListSensorComponent,
    DetailSensorComponent,
    AqiComponent,
    StationInfoComponent,
    SignUpComponent,
    EmailSubscriptionComponent,
    EqualValidator,
    MenuComponent,
    FooterComponent,
    SignInComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    AqiGraphComponent,
    CommentComponent,
    ListAlertComponent,
    ErrorLogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule,
    LeafletModule.forRoot(),
    FormsModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    NgProgressModule
  ],
  providers: [
    StationService,
    SensorService,
    EmailService,
    RegisterService,
    AuthService,
    LoginService,
    UserService,
    AlertService,
    CommentService,
    ErrorLoggingService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandlerService
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: NgProgressInterceptor,
      multi: true
    },
    GlobalErrorHandlerService,
    WINDOW_PROVIDERS,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
