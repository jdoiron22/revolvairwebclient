export enum ColorAqiEnum {
  max_green = 50,
  max_yellow = 100,
  max_orange = 150,
  max_red = 200,
  max_purple = 300,
  green = "#006966",
  yellow = "#FFDE33",
  orange = "#FF9933",
  red = "#CC0033",
  purple = "#660099",
  crimson = "#7E0033",
}