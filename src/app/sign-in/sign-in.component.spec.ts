import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {SignInComponent} from './sign-in.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Observable} from 'rxjs/Observable';
import {FormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EqualValidator} from '../validator/equal-validator.directive';
import {RouterTestingModule} from '@angular/router/testing';
import {LoginService} from '../services/login-service/login.service';
import {AuthService} from '../services/auth-service/auth.service';

describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;
  let loginService: LoginService;
  let authService: AuthService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        SignInComponent,
        EqualValidator
      ],
      providers: [
        LoginService,
        AuthService
      ],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        RouterTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(SignInComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    loginService = TestBed.get(LoginService);
    authService = TestBed.get(AuthService);
  }));

  afterEach(async(() => {
    component = null;
    fixture.destroy();
    loginService = null;
    authService = null;
  }));

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create loginService', () => {
    expect(loginService).toBeTruthy();
  });

  it('should create authService', () => {
    expect(loginService).toBeTruthy();
  });

  describe('onSubmit()', () => {
    it('should call loginService login method once', fakeAsync(() => {
      //Arrange
      spyOn(loginService, 'login').and.returnValue(Observable.of([]));
      spyOn((<any>component).router, 'navigate').and.returnValue(true);
      tick();

      //Act
      component.onSubmit();
      tick();

      //Assert
      expect(loginService.login).toHaveBeenCalledTimes(1);
    }));
  });

  describe('onSubmit()', () => {
    it('should go to list-station page if response successful', fakeAsync(() => {
      //Arrange
      const expected = ['list-station'];
      spyOn(loginService, 'login').and.returnValue(Observable.of([]));
      const spy = spyOn((<any>component).router, 'navigate').and.returnValue(true);
      tick();

      //Act
      component.onSubmit();
      tick();

      //Assert
      expect(spy).toHaveBeenCalledWith(expected);
    }));
  });
});
