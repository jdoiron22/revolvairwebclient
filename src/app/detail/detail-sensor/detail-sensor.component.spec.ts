import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {Observable} from 'rxjs/Observable';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgxPaginationModule} from 'ngx-pagination';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DetailSensorComponent} from './detail-sensor.component';
import {SensorService} from '../../services/sensor-service/sensor.service';
import {Sensor} from '../../models/sensor';
import {ActivatedRoute} from '@angular/router';
import {ObjectKeyService} from '../../services/object-key-service/object-key.service';

describe('DetailSensorComponent', () => {
  let component: DetailSensorComponent;
  const stationId = 900;
  const sensorId = 900;
  let fixture: ComponentFixture<DetailSensorComponent>;
  let sensorService : SensorService;
  let sensor : Sensor;
  let activatedRoute: Object;

  beforeEach(async(() => {
    sensor = new Sensor();
    sensor.id = sensorId;

    TestBed.configureTestingModule({
      declarations: [
        DetailSensorComponent
      ],
      providers: [
        SensorService,
      ],
      imports: [
        NgxPaginationModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(DetailSensorComponent);
      component = fixture.componentInstance;
      activatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
      activatedRoute["params"] = Observable.of({
        stationId: stationId,
        sensorId: sensorId});
      fixture.detectChanges();
    });

    sensorService = TestBed.get(SensorService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(async(() => {
    component = null;
    fixture.destroy();
    sensorService = null;
    sensor = null;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should get sensor from sensorService', fakeAsync(() => {
      //Arrange
      spyOn(sensorService, 'getSensor').and.returnValue(Observable.of([sensor]));
      tick();

      //Act
      component.ngOnInit();
      tick();

      //Assert
      expect(sensorService.getSensor).toHaveBeenCalled();
    }));

    it('should get latestValues from sensorService', fakeAsync(() => {
      //Arrange
      spyOn(sensorService, 'getLatestValues').and.returnValue(Observable.of([]));
      tick();

      //Act
      component.ngOnInit();
      tick();

      //Assert
      expect(sensorService.getLatestValues).toHaveBeenCalled();
    }));
  });


  describe('back()', () => {
    it('should go back', fakeAsync(() => {
      //Arrange
      let expected = ["detail-station", stationId];
      let spy = spyOn((<any>component).router, 'navigate');
      tick();

      //Act
      component.back();
      tick();

      //Assert
      expect(spy).toHaveBeenCalledWith(expected);
    }))
  });
});