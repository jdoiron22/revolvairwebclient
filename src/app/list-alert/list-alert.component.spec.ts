import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAlertComponent } from './list-alert.component';
import {AlertService} from '../services/alert-service/alert.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Observable} from 'rxjs/Observable';
import {Alert} from '../models/alert';
import {tick} from '@angular/core/testing';
import {fakeAsync} from '@angular/core/testing';
import {Station} from '../models/station';

describe('ListAlertComponent', () => {
  let component: ListAlertComponent;
  let fixture: ComponentFixture<ListAlertComponent>;
  let alertService: AlertService;
  let alert: Alert;
  let station: Station;
  const stationId = 9999;

  beforeEach(fakeAsync(() => {
    alert = new Alert(null, null, '', '');
    station = new Station();
    station.id = stationId;

    TestBed.configureTestingModule({
      declarations: [
        ListAlertComponent
      ],
      providers: [
        AlertService
      ],
      imports: [
       HttpClientTestingModule,
       RouterTestingModule
      ]
    })
    .compileComponents();
    alertService = TestBed.get(AlertService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should get alerts from alertService', fakeAsync(() => {
      // Arrange
      spyOn(alertService, 'getAlerts').and.returnValue(Observable.of([alert]));
      tick();

      // Act
      component.ngOnInit();
      tick();

      // Assert
      expect(alertService.getAlerts).toHaveBeenCalled();
    }));
  });

  describe('alertStation(id: number)', () => {
    it('should go to the station details page', fakeAsync(() => {
      // Arrange
      const expected = ['detail-station', station.id];
      const spy = spyOn((<any>component).router, 'navigate');
      tick();

      // Act
      component.alertStation(station.id);
      tick();

      // Assert
      expect(spy).toHaveBeenCalledWith(expected);
    }));
  });

});
