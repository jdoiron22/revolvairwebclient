import { Component, OnInit } from '@angular/core';
import { AlertService} from '../services/alert-service/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-alert',
  templateUrl: './list-alert.component.html',
  styleUrls: ['./list-alert.component.css']
})
export class ListAlertComponent implements OnInit {
  alerts = [];

  constructor(private alertService: AlertService, private router: Router) { }

  ngOnInit() {
    this.getAlerts();
  }
  getAlerts(): void {
    this.alertService.getAlerts().subscribe(
      alerts => this.alerts = alerts);
  }

  alertStation(id: number) {
    this.router.navigate(['detail-station', id]);
  }
}
