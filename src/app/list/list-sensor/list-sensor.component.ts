import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';
import {SensorService} from '../../services/sensor-service/sensor.service';
import {Sensor} from '../../models/sensor';
import {ObjectKeyService} from '../../services/object-key-service/object-key.service';

@Component({
  selector: 'list-sensor',
  templateUrl: './list-sensor.component.html',
  styleUrls: ['./list-sensor.component.css'],
})
export class ListSensorComponent implements OnInit {

  constructor(
    private sensorService : SensorService,
    private route: ActivatedRoute,
    private router : Router
  ) {
    this.route.params.subscribe( params => this.params = params);
  }

  observable: Observable<Sensor[]>;
  params: Object;
  currentPage: number;
  private itemsPerPage = 6;
  private getObjectKeys = ObjectKeyService.getObjectKeys;

  ngOnInit() {
    this.observable = this.sensorService.getSensors(this.params["id"]);
  }

  pageChanged(page: number) {
    this.currentPage = page;
  }

  details(sensorId: number){
    this.router.navigate(['detail-station', this.params["id"], 'detail-sensor', sensorId])
  }

}
