import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Comment} from '../../models/comment';

@Injectable()
export class CommentService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  saveComment(text: string, stationId: number): Observable<Comment> {
    const url = `${this.apiUrl}/stations/${stationId}/comments`;
    return this.http.post<Comment>(url, {
      text: text,
    }, {observe: 'body'});
  }

  getComments(stationId: number): Observable<Comment[]> {
    const url = `${this.apiUrl}/stations/${stationId}/comments`;
    return this.http.get<Comment[]>(url);
  }
}
