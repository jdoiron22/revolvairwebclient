import {TestBed, inject, async} from '@angular/core/testing';

import { CommentService } from './comment.service';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('CommentService', () => {
  let commentService: CommentService;
  const apiUrl = environment.apiUrl;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [CommentService]
    });
    commentService = TestBed.get((CommentService));
  });
  it('should be created', inject([CommentService], (service: CommentService) => {
    expect(service).toBeTruthy();
  }));

  describe('getComments()', () => {
    it('should_get_all_comments_by_station_id', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          // Arrange
          const stationId = 1;
          const expectedUrl = apiUrl + '/stations/' + stationId + '/comments';
          // Act
          commentService.getComments(stationId).subscribe();
          // Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          });
        })));
  });
  describe('saveComment()', () => {
    it('should_save_comment_with_text_and_good_station_id', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          // Arrange
          const textComment = 'Test de sauvegarde';
          const stationId = 1;
          const expectedUrl = apiUrl + '/stations/' + stationId + '/comments';
          // Act
          commentService.saveComment(textComment, stationId).subscribe();
          // Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'POST'
          });
        })));
  });
});
