import { TestBed, inject, async } from '@angular/core/testing';

import { AlertService } from './alert.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

describe('AlertService', () => {
  let alertService: AlertService;
  const apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AlertService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    alertService = TestBed.get(AlertService);
  });

  it('should be created', inject([AlertService], (service: AlertService) => {
    expect(service).toBeTruthy();
  }));

  describe('getAlerts()', () => {
    it('should get all alerts from apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
        // Arrange
        const expectedUrl = apiUrl + '/alerts';

        // Act
        alertService.getAlerts().subscribe();

        // Assert
        backend.expectOne({
          url: expectedUrl,
          method: 'GET'
        });
        })
    ));
  });
});
