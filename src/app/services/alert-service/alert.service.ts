import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { Alert } from '../../models/alert';

@Injectable()
export class AlertService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAlerts(): Observable<Alert[]> {
    const url = `${this.apiUrl}/alerts`;
    return this.http.get<Alert[]>(url).map(res => res['data']);
  }
}
