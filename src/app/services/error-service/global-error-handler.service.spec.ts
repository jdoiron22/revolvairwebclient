import {TestBed, inject, tick, fakeAsync} from '@angular/core/testing';

import { GlobalErrorHandlerService } from './global-error-handler.service';
import { WINDOW_PROVIDERS } from "../window-service/window.service";
import { ErrorLoggingService } from "./error-logging.service";
import {Observable} from "rxjs/Observable";
import {ErrorLog} from "../../models/error-log";
import {HttpClient, HttpClientModule, HttpHandler} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('GlobalErrorHandlerService', () => {
  let errorLogginService: ErrorLoggingService;
  let globalErrorHandler: GlobalErrorHandlerService;
  let errorLog: ErrorLog;

  beforeEach(() => {
    errorLog = new ErrorLog(null,
      '',
      '',
      '',
      '',
      null,
      '');
    TestBed.configureTestingModule({
      providers: [GlobalErrorHandlerService,
        ErrorLoggingService,
        HttpClientTestingModule,
        HttpClient,
        HttpHandler,
        WINDOW_PROVIDERS],
      imports: [
        HttpClientTestingModule,
        HttpClientModule
      ]
    });
    errorLogginService = TestBed.get(ErrorLoggingService);
    globalErrorHandler = TestBed.get(GlobalErrorHandlerService);
  });

  it('should be created', inject([GlobalErrorHandlerService], (service: GlobalErrorHandlerService) => {
    expect(service).toBeTruthy();
  }));

  it('should create errorLoggingServuce', () => {
    expect(errorLogginService).toBeTruthy();
  });

  it('should create window token', () => {
    expect(window).toBeTruthy();
  });

});
