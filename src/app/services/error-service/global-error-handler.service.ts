import { ErrorHandler, Injectable, Inject} from '@angular/core';
import { WINDOW } from '../window-service/window.service';
import { ErrorLog } from '../../models/error-log';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorLoggingService } from "./error-logging.service";

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {

  private statusCode;
  private model : ErrorLog;
  private userId: number;
  private clientError: Object;

  constructor(
    @Inject(WINDOW) private window: Window,
    public errorLogging: ErrorLoggingService
  ) { }

  /* takes error information and formats it
  to send it to server via errorLoggingService  */
  handleError(error: any) {
    this.statusCode = 0;
    if (error instanceof HttpErrorResponse){
      this.statusCode = error.status;
    }
    this.clientError = error;

    this.errorLogging.getUserId().subscribe(userId => this.userId = userId,
      error => console.log(error),
      () => this.onComplete());
  }

  onComplete()
  {
    this.updateModel(this.clientError);
    this.errorLogging.log(this.model).subscribe();
  }

  updateModel(error: any) {
    this.model = new ErrorLog(
      this.statusCode,
      error.message,
      this.window.location.toString(),
      '',
      '',
      this.userId,
      '');
  }
}
