import {TestBed, inject, async} from '@angular/core/testing';

import { ErrorLoggingService } from './error-logging.service';
import { environment } from "../../../environments/environment";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { ErrorLog } from "../../models/error-log";

describe('ErrorLoggingService', () => {
  let errorLoggingService: ErrorLoggingService;
  const apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [ErrorLoggingService]
    });
    errorLoggingService = TestBed.get(ErrorLoggingService);
  });

  it('should be created', inject([ErrorLoggingService], (service: ErrorLoggingService) => {
    expect(service).toBeTruthy();
  }));

  describe('send()', () => {
    it('should post new error log to apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          const expectedUrl = apiUrl + '/logs';
          const log = new ErrorLog(500,
            'message',
            'url',
            '',
            '',
            0,
            '');

          //Act
          errorLoggingService.send(log).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'POST'
          });
        })));
  });

  describe('getLogs()', () => {
    it('should_get_all_logs', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          // Arrange
          const expectedUrl = apiUrl + '/logs';

          // Act
          errorLoggingService.getLogs().subscribe();

          // Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          });
        })));
  });

  describe('getUserId()', () => {
    it('should_get_id_from_user', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          // Arrange
          const expectedUrl = apiUrl + '/userIdLogs';

          // Act
          errorLoggingService.getUserId().subscribe();

          // Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          });
        })));
  });

});
