import { Injectable } from '@angular/core';
import { ErrorLog } from "../../models/error-log";
import {HttpClient, HttpHeaders } from "@angular/common/http";
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {catchError} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
  observe: 'body', })
};

@Injectable()
export class ErrorLoggingService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }


  public log(errorLog: ErrorLog) {
    console.log(errorLog.errorMessage + ';\n' + errorLog.url + ';\n' + errorLog.statusCode + ';');
    return this.send(errorLog);
  }

  send(errorLog: ErrorLog): Observable<Object>
  {
    const url = `${this.apiUrl}/logs`;
    return this.http.post<Object>(url, {
      statusCode: errorLog.statusCode,
      errorMessage: errorLog.errorMessage,
      url: errorLog.url,
      userId: errorLog.userId
    }, httpOptions);
  }

  getLogs(): Observable<ErrorLog[]> {
    const url = `${this.apiUrl}/logs`;

    return this.http.get<ErrorLog[]>(url)
      .map(res => res['data'])
      .pipe(
        catchError(this.handleError([]))
    );
  }

  getUserId(): Observable<number>{
    const url = `${this.apiUrl}/userIdLogs`;

    return this.http.get<number>(url);
  }

  private handleError<T> (result?: T) {
    return (): Observable<T> => {
      return Observable.of(result as T);
    };
  }
}
