import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { MatSnackBar } from "@angular/material";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { GlobalErrorHandlerService } from "./global-error-handler.service";

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(
    public errorHandler: GlobalErrorHandlerService,
    private snackbar: MatSnackBar,
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).do((event: HttpEvent<any>) => {}, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        this.popMessage(err);
        this.errorHandler.handleError(err);
      }
    });
  }

  popMessage(err) {
    this.snackbar.open(err.message, 'close', {
      duration: 3000
    });
  }
}
