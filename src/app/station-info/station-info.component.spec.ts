import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { StationInfoComponent } from './station-info.component';
import {Observable} from 'rxjs/Observable';
import {StationService} from '../services/station-service/station.service';
import {Station} from '../models/station';
import {DetailStationComponent} from '../detail/detail-station/detail-station.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgxPaginationModule} from 'ngx-pagination';

describe('StationInfoComponent', () => {
  let component: StationInfoComponent;
  let fixture: ComponentFixture<StationInfoComponent>;
  let stationService: StationService;
  let station: Station;
  const stationId = 900;

  beforeEach(async(() => {
    station = new Station();
    station.id = stationId;
    TestBed.configureTestingModule({
      declarations: [
          StationInfoComponent
      ],
      providers: [
          StationService,
      ],
      imports: [
          NgxPaginationModule,
          HttpClientTestingModule,
          RouterTestingModule
      ]
    })
    .compileComponents().then(() => {
        fixture = TestBed.createComponent(StationInfoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

      stationService = TestBed.get(StationService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(async(() => {
      component = null;
      fixture.destroy();
      stationService = null;
      station = null;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

    describe('ngOnInit()', () => {
        it('station info should get station from stationService', fakeAsync(() => {
            // Arrange
            spyOn(stationService, 'getStation').and.returnValue(Observable.of([station]));
            tick();

            // Act
            component.ngOnInit();
            tick();

            // Assert
            expect(stationService.getStation).toHaveBeenCalled();
        }));
    });

});
