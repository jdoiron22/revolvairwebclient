import { Component, OnInit } from '@angular/core';
import {StationService} from '../services/station-service/station.service';
import {Observable} from 'rxjs/Observable';
import {Station} from '../models/station';
import {ActivatedRoute, Params} from '@angular/router';
import {ObjectKeyService} from '../services/object-key-service/object-key.service';

@Component({
  selector: 'app-station-info',
  templateUrl: './station-info.component.html',
  styleUrls: ['./station-info.component.css']
})
export class StationInfoComponent implements OnInit {

  constructor(
      private stationService : StationService,
      private route: ActivatedRoute
  ) {this.route.params.subscribe( params => this.params = params); }

    params: Params;
    station: Station;
    private getObjectKeys = ObjectKeyService.getObjectKeys;

  ngOnInit() {
      this.stationService.getStation(this.params['id']).subscribe(
          station => this.station = station
      );
  }

}
