import {Component, OnInit, ViewChild} from '@angular/core';
import {CommentService} from '../services/comment-service/comment.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Comment} from '../models/comment';
import {MatSnackBar} from '@angular/material';
const errorMessageConnection = 'Vous devez être connecté pour envoyer un commentaire !';
const sendingMsgCommentSucces = 'Commentaire sauvegardé !';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @ViewChild('newCommentForm') form;
  params: Params;
  comment: Comment = new Comment('', null, null, '');
  comments = [];
  constructor(private commentService: CommentService,
              private route: ActivatedRoute,
              public snackBar: MatSnackBar) {this.route.params.subscribe( params => this.params = params);
  }
  ngOnInit() {
    this.getComments();
  }

  onSubmit() {
    this.commentService.saveComment(this.comment.text, this.params['id']).subscribe(
      comment => this.comment = comment,
      error => this.onErrorSave(error) ,
      () => this.onCompleteSave()
    );
  }

  getComments(): void {
    this.commentService.getComments(this.params['id']).subscribe(
      comments => this.comments = comments,
      error => console.error(error),
      () => console.log('Commentaires chargés')
    );
  }
  onErrorSave(error) {
    this.snackBar.open(errorMessageConnection, 'Close', {
      duration: 3000
    });
    console.error(error);
  }
  onCompleteSave() {
    this.snackBar.open(sendingMsgCommentSucces, 'Close', {
      duration: 3000
    });
    console.log('Commentaire sauvegardé');
    this.form.reset();
    this.getComments();
  }
}
