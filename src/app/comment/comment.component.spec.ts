import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { CommentComponent } from './comment.component';
import {CommentService} from '../services/comment-service/comment.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NgxPaginationModule} from 'ngx-pagination';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material';
import {Station} from '../models/station';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Comment} from '../models/comment';
import {Observable} from 'rxjs/Observable';

describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;
  let commentService: CommentService;
  const stationId = 900;
  let station: Station;
  let comment: Comment;

  beforeEach(fakeAsync(() => {
    station = new Station();
    station.id = stationId;
    comment = new Comment('', null, null, '');

    TestBed.configureTestingModule({
      declarations: [
        CommentComponent,

      ],
      providers: [
        CommentService,
      ],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        RouterTestingModule,
        MatSnackBarModule,
        NgxPaginationModule,
        BrowserAnimationsModule
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(CommentComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
    commentService = TestBed.get(CommentService);
  }));
  afterEach(fakeAsync(() => {
    component = null;
    fixture.destroy();
    commentService = null;
    comment = null;
  }));
  it('should create', fakeAsync(() => {
    tick(1000);
    expect(component).toBeTruthy();
  }));

  describe('ngOnInit()', () => {
    it('should get comments from commentService', fakeAsync(() => {
      // Arrange
      spyOn(commentService, 'getComments').and.returnValue(Observable.of([comment]));
      tick();

      // Act
      component.ngOnInit();
      tick();

      // Assert
      expect(commentService.getComments).toHaveBeenCalled();
    }));
  })

  describe('onSubmit()', () => {
    it('should save comment from commentService and getComment after ', fakeAsync(() => {
      // Arrange
      spyOn(commentService, 'saveComment').and.returnValue(Observable.of([comment]));
      spyOn(commentService, 'getComments').and.returnValue(Observable.of([comment]));
      tick();

      // Act
      component.onSubmit();
      tick(10000);

      // Assert
      expect(commentService.saveComment).toHaveBeenCalled();
      expect(commentService.getComments).toHaveBeenCalled();
    }));
  });
});
