import { Component, OnInit } from '@angular/core';
import { ErrorLog } from '../models/error-log';
import {ErrorLoggingService} from "../services/error-service/error-logging.service";

@Component({
  selector: 'app-error-log',
  templateUrl: './error-log.component.html',
  styleUrls: ['./error-log.component.css']
})
export class ErrorLogComponent implements OnInit {

  logs: ErrorLog[];
  userId: number;

  constructor(private errorLoggingService: ErrorLoggingService) { }

  getLogs(): void {
    this.errorLoggingService.getUserId().subscribe(userId => this.userId = userId);
    this.errorLoggingService.getLogs().subscribe(logs => this.logs = logs,
        error => console.log(error),
      () => this.onComplete());
  }

  ngOnInit() {
    this.getLogs();
  }

  onComplete() {
    console.log(this.logs);
  }
}
