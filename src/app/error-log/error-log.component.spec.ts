import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { ErrorLogComponent } from './error-log.component';
import {ErrorLoggingService} from "../services/error-service/error-logging.service";
import {ErrorLog} from "../models/error-log";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Observable} from "rxjs/Observable";

describe('ErrorLogComponent', () => {
  let component: ErrorLogComponent;
  let fixture: ComponentFixture<ErrorLogComponent>;
  let errorService: ErrorLoggingService;
  let errorLog: ErrorLog;

  beforeEach(fakeAsync(() => {
    errorLog = new ErrorLog(null,
      '',
      '',
      '',
      '',
      null,
      '');
    TestBed.configureTestingModule({
      declarations: [ ErrorLogComponent ],
      providers: [ ErrorLoggingService ],
      imports: [ HttpClientTestingModule]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(ErrorLogComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
    errorService = TestBed.get(ErrorLoggingService);
  }));

  afterEach(fakeAsync(() => {
    component = null;
    fixture.destroy();
    errorService = null;
    errorLog = null;
  }));

  it('should create', fakeAsync(() => {
    tick(1000);
    expect(component).toBeTruthy();
  }));

  describe('ngOnInit()', () => {
    it('should get logs from errorLoggingService', fakeAsync(() => {
      // Arrange
      spyOn(errorService, 'getLogs').and.returnValue(Observable.of([errorLog]));
      tick();

      // Act
      component.ngOnInit();
      tick();

      // Assert
      expect(errorService.getLogs).toHaveBeenCalled();
    }));
  });

  describe('ngOnInit()', () => {
    it('should get user id from errorLoggingService', fakeAsync(() => {
      // Arrange
      spyOn(errorService, 'getUserId').and.returnValue(Observable.of());
      tick();

      // Act
      component.ngOnInit();
      tick();

      // Assert
      expect(errorService.getUserId).toHaveBeenCalled();
    }));
  })

});
