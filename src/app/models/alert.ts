export class Alert {
  constructor(
    public id: number,
    public station_id: number,
    public newAQI: string,
    public created_at: string,
  ) { }
}
