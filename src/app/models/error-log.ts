export class ErrorLog {
  constructor(
    public statusCode: number,
    public errorMessage: string,
    public url: string,
    public remoteIpAddress: string,
    public HTTPXIpAddress: string,
    public userId: number,
    public created_at: string,
    ) { }
}
