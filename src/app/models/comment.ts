export class Comment {
  constructor(
    public text: string,
    public name: string,
    public station_id: number,
    public created_at: string
  ) {  }
}
