import {Aqi} from './aqi';
import {StationInfo} from './station-info';

export class Station {
    id: number;
    is_private: number;
    aqi: Aqi;
    info: StationInfo;
    raw_data:Array<number[]>;
}